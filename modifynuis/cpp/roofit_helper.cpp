#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooWorkspace.h"
#include <algorithm>

namespace RooFitHelper {

  void removeArgs(RooArgSet & set, const std::vector<std::string> & removeNames);
  void getBasePdf(RooProdPdf* pdf, RooArgSet& set);
  RooSimultaneous * get_pdf(RooWorkspace * nW, 
			    const RooSimultaneous * pdf, 
			    const std::vector<std::string> & additionalNames, 
			    const std::vector<std::string> & removeNames)
  {
    if (additionalNames.size() != 0)
      std::cout<<"\t Adding additional constraint terms"<<std::endl;
    for (const auto & name :additionalNames)
      std::cout << "\t \t constraint: " << name << std::endl;

    RooAbsCategoryLValue*  m_cat = (RooAbsCategoryLValue*)&pdf->indexCat();
    int numChannels = m_cat->numBins(0);
    
    std::map<std::string, RooAbsPdf*> pdfMap;
    for ( int i= 0; i < numChannels; i++ ) {
      m_cat->setBin(i);
      RooAbsPdf* pdfi = dynamic_cast<RooAbsPdf*>(pdf->getPdf(m_cat->getLabel()));
      RooArgSet baseComponents;
      if(typeid( *pdfi) == typeid(RooProdPdf)){
	RooFitHelper::getBasePdf((RooProdPdf*)pdfi, baseComponents);
	for (const auto & name :additionalNames)
	  {
	    RooAbsPdf* pdf_tmp  =  nW->pdf(name.c_str());
	    assert(pdf_tmp);
	    baseComponents.add(*pdf_tmp);
	  }

      }
      else{
	baseComponents.add(*pdfi);
      }
      
      removeArgs(baseComponents, removeNames);
      std::string newPdfName = std::string(pdfi->GetName()) + "_new";
      pdfi = new RooProdPdf(newPdfName.c_str(), newPdfName.c_str(), baseComponents);
      
      pdfMap[m_cat->getLabel()] = pdfi;
    }
    
    RooSimultaneous* newPdf = new RooSimultaneous(pdf->GetName(), pdf->GetTitle(), pdfMap, *m_cat);

    return newPdf;

  }

  void getBasePdf(RooProdPdf* pdf, RooArgSet& set)
  {
    RooArgList pdfList = pdf->pdfList();
    int pdfSize = pdfList.getSize();
    if (pdfSize==1)
      {
	set.add(pdfList);
      }
    else
      {
	TIterator* iter = pdfList.createIterator();
	RooAbsArg* arg;
	while ((arg = (RooAbsArg*)iter->Next()))
	  {
	    std::string className = arg->ClassName();
	    if (className != "RooProdPdf")
	      {
		set.add(*arg);
	      }
	    else
	      {
		RooProdPdf* thisPdf = dynamic_cast<RooProdPdf*>(arg);
		assert ( thisPdf );
		getBasePdf(thisPdf, set);
	      }
	  }
	delete iter; iter = NULL;
      }
  }
  void removeArgs(RooArgSet & set, const std::vector<std::string> & removeNames)
  {

    if (removeNames.size() != 0)
      std::cout<<"\t Removing constraint terms"<<std::endl;
    for (const auto & name :removeNames)
      std::cout << "\t \t constraint: " << name << std::endl;
    TIterator* iter = set.createIterator();
    RooAbsArg* arg;
    while ((arg = (RooAbsArg*)iter->Next())) {
      std::string arg_name = (std::string)arg->GetName();
      if (std::find(removeNames.begin(), removeNames.end(), arg_name) != removeNames.end()) {
	std::cout << "remove" << arg_name << std::endl;
	set.remove(*arg);
      }
    }
  }

  void list_to_map(RooArgList & inPdfList, RooAbsCategoryLValue & inIndexCat)
  {
    if (inPdfList.getSize() != inIndexCat.numTypes()) {
      std::cerr << " ERROR: Number PDF list entries must match number of index category states, no PDFs added" << std::endl;

      return ;
    }

    map<string,RooAbsPdf*> pdfMap ;
    // Iterator over PDFs and index cat states and add each pair
    auto* pIter = inPdfList.createIterator() ;
    TIterator* cIter = inIndexCat.typeIterator() ;
    RooAbsPdf* pdf ;
    RooCatType* type(0) ;

    while ((pdf=(RooAbsPdf*)pIter->Next())) {
      type = (RooCatType*) cIter->Next() ;
      std::cout << string(type->GetName()) << ": " << pdf->GetName() << std::endl;

      pdfMap[string(type->GetName())] = pdf;
    }
    delete pIter ;
    delete cIter ;
  }


}
