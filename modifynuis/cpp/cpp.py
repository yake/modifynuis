__all__ = [
    '_load_run_sig',
    '_load_roofit_helper',
    ]


import ROOT


def _load_run_sig():
    try:
        ROOT.gSystem.CompileMacro('modifynuis/cpp/run_sig.cpp', 'k-', '', 'cache')
        print 'modifyNuis: \t run_sig loaded!'
    except:
        raise RuntimeError

    try:
        ROOT.Result()
    except:
        print "run_sig not compiled"

def _load_roofit_helper():
    try:
        ROOT.gSystem.CompileMacro('modifynuis/cpp/roofit_helper.cpp', 'k-', '', 'cache')
        print 'modifyNuis: \t roofit helper loaded!'
    except:
        raise RuntimeError
