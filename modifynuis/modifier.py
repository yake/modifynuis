import ROOT


def modify_workspace(
        old_ws, 
        nps_to_add, 
        exprs_to_add, 
        nps_to_remove, 
        constraints_to_remove=None, 
        globs_to_remove=None,
        model_config_name='ModelConfig',
        snapshots=None,
        debug=False):
    """
    """

    _nps_to_remove = nps_to_remove

    if globs_to_remove != None:
        _globs_to_remove = globs_to_remove
    else:
        _globs_to_remove = ['nom_' + _np for _np in _nps_to_remove]

    if constraints_to_remove != None:
        _constraints_to_remove = constraints_to_remove
    else:
        _constraints_to_remove = [_np + 'Constraint' for _np in _nps_to_remove]

    # check that the nps, global observables and constraints we plan to remove
    # actually are there in the input workspace
    check_nps(old_ws, _nps_to_remove, _globs_to_remove, _constraints_to_remove)

    _nps_to_add = nps_to_add
    _exprs_to_add = exprs_to_add


    if len(_exprs_to_add) != len(_nps_to_remove):
        raise ValueError('not the same number of expressions and nps to remove ({} != {})'.format(
                len(_exprs_to_add), len(_nps_to_remove)))

    # retrieve model config
    old_model_config = old_ws.obj(model_config_name)
    ws_name = old_ws.GetName()
    old_ws.SetName('oldWS')

    # define new workspace and ModelConfig
    new_ws = ROOT.RooWorkspace(ws_name)
    new_model_config = ROOT.RooStats.ModelConfig(model_config_name, new_ws)
    new_model_config.SetWS(new_ws)
    
    # import data
    for _data in old_ws.allData():
        getattr(new_ws, 'import')(_data)


    # global observables
    _globs = old_model_config.GetGlobalObservables().createIterator()
    _glob = _globs.Next()
    _new_globs = ROOT.RooArgSet()
    print('modifyNuis: --> copying global observables') 
    while _glob:
        if _glob.GetName() in _globs_to_remove:
            print '\t removing global obs: ', _glob.GetName(), type(_glob), _glob.getVal()
        else:
            _new_globs.add(_glob)
        _glob = _globs.Next()

    # Adding global obseervables. For some reason (probably garbage collection), 
    # we need to store them in a list before putting them in the RooArgSet
    _rrvs_globs = []
    for _np in _nps_to_add:
        _glob_name = 'nom_' + _np
        if old_ws.var(_glob_name):
            print 'Variable {} is already in the workspace.'.format(_glob_name)
        else:
            _rrvs_globs.append(ROOT.RooRealVar(_glob_name, _glob_name, 0))
    for _r in _rrvs_globs:
        _new_globs.add(_r)
    new_model_config.SetGlobalObservables(_new_globs)
    # ---
    
    # nuisance parameters (alphas and gammas)
    _nps = old_model_config.GetNuisanceParameters().createIterator()
    _np = _nps.Next()
    _new_nps = ROOT.RooArgSet()
    print('modifyNuis: --> copying nuisance parameters') 
    while _np:
        if _np.GetName() in _nps_to_remove:
            print '\t removing nuisance param: ', _np.GetName(), type(_np), _np.getVal()
        else:
            _new_nps.add(_np)
        _np = _nps.Next()
    
    # Adding NPs. For some reason (probably garbage collection), 
    # we need to store them in a list before putting them in the RooArgSet
    _rrvs_nps = []
    for _np in _nps_to_add:
        if old_ws.var(_np):
            print 'Variable {} is already in the workspace.'.format(_np)
        else:
            _rrvs_nps.append(ROOT.RooRealVar(_np, _np, 0, -5, 5))
    for _r in _rrvs_nps:
        _new_nps.add(_r)
    new_model_config.SetNuisanceParameters(_new_nps)
    # ----

    # observables
    _observables = old_model_config.GetObservables().createIterator()
    _obs = _observables.Next()
    _new_observables = ROOT.RooArgSet()
    print('modifyNuis: --> copying observables') 
    while _obs:
        _new_observables.add(_obs)
        _obs = _observables.Next()
    new_model_config.SetObservables(_new_observables)
    # ----

    # Adding new gaussian constraints
    print('modifyNuis: --> Adding new gaussian constraints')
    _additional_names = ROOT.vector('std::string')()
    for _np in _nps_to_add:
        _constraint = 'RooGaussian::ConstraintPdf_{0}({0}, nom_{0}, 1)'.format(_np)
        print('modifyNuis:\t {}'.format(_constraint))
        new_ws.factory(_constraint)
        _additional_names.push_back('ConstraintPdf_' + _np)

    # -- for now implement by hand
    for _expression in _exprs_to_add:
        new_ws.factory(_expression)

    print('modifyNuis: --> Removing old gaussian constraints')
    _remove_names = ROOT.vector('std::string')()
    ## need to check how to make this work
#     for _constraint in _constraints_to_remove:
#         _remove_names.push_back(_constraint)

    # --- pdf building
    print('modifyNuis: --> Creating new PDF')
    old_pdf = old_model_config.GetPdf()
    new_pdf = ROOT.RooFitHelper.get_pdf(new_ws, old_pdf, _additional_names, _remove_names)
    
    # importing a constant to alleviate impact of the gaussian constraints associated
    # with the NPs we are removing
    new_ws.factory('one[1]')

    # import newly built pdf to the workspace
    _nps_from_exprs = [_expr.split('::')[1].split('(')[0] for _expr in _exprs_to_add]
    _old_str, _new_str = generate_renaming_expressions(_nps_to_remove, _constraints_to_remove, _nps_from_exprs, constant_name='one')
    print('modifyNuis: --> Old variable list = {}'.format(_old_str))
    print('modifyNuis: --> New variable list = {}'.format(_new_str))
    if debug:
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.DEBUG)
    getattr(new_ws, 'import')(
        new_pdf,
        ROOT.RooFit.RenameVariable(_old_str, _new_str),
        ROOT.RooFit.RecycleConflictNodes())
        # ROOT.RooFit.Silence())
    ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)

    # ---
    print('modifyNuis: --> new pdf loaded to the workspace')

    # model config setup and import to the workspace
    new_model_config.SetPdf(new_pdf)
    new_model_config.SetParametersOfInterest(old_model_config.GetParametersOfInterest())
    getattr(new_ws, 'import')(new_model_config)
    # ---

    # do something about snap shots ?
    # to be debuggged!
    if snapshots != None:
        if not isinstance(snapshots, (list, tuple)):
            _snapshots = [snapshots]
        else:
            _snapshots = snapshots

        for _snap in _snapshots:
            if (old_ws.loadSnapshot(_snap)):
                new_ws.saveSnapshot(_snap, new_model_config.GetNuisanceParameters(), True)
    

    new_ws.importClassCode()
    new_ws.writeToFile('new.root')


def generate_renaming_expressions(old_nps, old_constraints, new_nps, constant_name='one'):
    """
    """
    if len(old_nps) != len(new_nps):
        raise ValueError('old and new nps have different lengths ({} != {})!'.format(
                len(old_nps), len(new_nps)))

    if len(old_nps) != len(old_constraints):
        raise ValueError('old nps and old constraints have different lengths ({} != {})!'.format(
                len(old_nps), len(old_constraints)))
    
    _ones = ['one' for i in xrange(len(old_constraints))]
    
    _old_list = old_nps + old_constraints
    _new_list = new_nps + _ones
    _old_str = ','.join(_old_list)
    _new_str = ','.join(_new_list)
    return _old_str, _new_str

def check_nps(ws, nuisance_parameters, global_observables, gaussian_constraints):
    """
    """
    if len(nuisance_parameters) != len(global_observables):
        raise ValueError('len(nuisance_parameters) != len(global_observables) ({} != {})'.format(
                 len(nuisance_parameters), len(global_observables)))

    if len(nuisance_parameters) != len(gaussian_constraints):
        raise ValueError('len(nuisance_parameters) != len(gaussian_constraints) ({} != {})'.format(
                 len(nuisance_parameters), len(gaussian_constraints)))

    for _np_name in nuisance_parameters:
        _np = ws.var(_np_name)
        try:
            _np.GetName()
        except:
            raise NameError('{} NP constraint not in workspace!'.format(_np_name))

    for _glob_name in global_observables:
        _glob = ws.var(_glob_name)
        try:
            _glob.GetName()
        except:
            raise NameError('{} global observable not in workspace!'.format(_glob_name))

    for _constraint in gaussian_constraints:
        _pdf = ws.pdf(_constraint)
        try:
            _pdf.GetName()
        except:
            raise NameError('{} constraint not in workspace!'.format(_constraint))
