Note that the latest version of recomposition tool is moved to: https://gitlab.cern.ch/atlas_higgs_combination/software/btaggingnpmodification/-/tree/master

# Initial setup
Run the following commands to initialise the project (the initial setup will be greatly simplified once the tool will be in an official release).

```
mkdir evr_tool_dev
cd evr_tool_dev
setupATLAS
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
mkdir build  run
cd athena/
git remote -v
git remote add yake https://:@gitlab.cern.ch:8443/yake/athena.git
git fetch yake
git checkout -b  21.2_eigenvectorrecompositiontool yake/21.2_eigenvectorrecompositiontool --no-track
git atlas addpkg FTagAnalysisInterfaces
git atlas addpkg xAODBTaggingEfficiency
git atlas addpkg CalibrationDataInterface

cd ../build
asetup 21.2,AnalysisBase,latest,here
cmake ../athena/Projects/WorkDir/
make
source x86_64-centos7-gcc8-opt/setup.sh
cd ../run
BTaggingEigenVectorRecompositionToolTester
cd ..
git clone https://gitlab.cern.ch/yake/modifynuis.git
cd modifynuis
git checkout -b prod origin/prod
```

# Setup at each login
From the `modifynuis` directory:

```
source setup_lxplus.sh
```

# Test Run on VHbb workspaces

## Modify the workspace
```
./modify vhbb/vhbb_unblinding.root
```

## Fit the modified WS for a quick test
```
export IS_BLINDED=0
./fit new.root
```

